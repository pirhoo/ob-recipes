Title: About
Summary: 
Date: 2016-06-28 08:50:28
Author: nkb
Category: page
Slug: about
Lang: en

This websites provides a series of tutorials for journalists working with budget and spending data. It lets them understand the data and use it for possible story leads.

Corruption takes many forms. In the European Union, cases of plain bribery (a public servant asking for cash to perform an operation) are fairly rare. However, more complex cases of misuse of public funds are abundant. (Read [A Clean House?](http://www.nordicacademicpress.com/bok/a-clean-house/) page 12 for a more detailed discussion of 'sophisticated' corruption.) These tutorials let readers understand the sophistication of European processes in bribery, embezzlement, favoritism and general abuse of public resources for private gains.

The format of the tutorials is satirical: it pretends that they are made for civil servants hoping to use public funds for personal profit. We chose satire as a tutorial genre to entice readers into actually reading the tutorials. Analyzing budget data is a rather coarse field, which can make use of a lighter tone.


## Contribute

If you feel like writing and sharing a recipe, you are more than welcome! Contact us at chef@cookingbudgets.com and we'll help you get started.