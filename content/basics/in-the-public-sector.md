Title: In the public sector
Summary: How public administrations account for their revenues and expenses.
Date: 2015-02-12 08:50:28
Author: nkb
Category: Basics
Slug: public-sector
Lang: en


## Local administration

Local administrations count money flows much like companies do. They usually get their income from taxes and spend money on the salaries of civil servants, like teachers, police officers, on the things they need to provide public services (buses for public transport), and on companies that carry out some of the work (a construction company that builds a highway).

![Image](../basics/image02.png)

There are many things that local administrations must provide no matter what, such as education, transport, policing etc. Citizens, for their part, must pay taxes. To represent this continuous cycle, public budgets use the terms “operating income” and “operating expenditures” (sometimes, “revenue” is used instead of “operating”).

Local administrations also have to build swimming pools and schools. These are called “investment expenditures” (sometimes “capital expenditures”). This can be financed by the surplus from the operations.

![Image](../basics/image03.png)

Most of the time, taxes do not suffice to pay for all investments. Local administrations can go to the bank to borrow money. In most countries, it is forbidden for a local administration to borrow money to cover operational expenditures. Borrowing for investment is allowed, borrowing for everyday operations is not.

![Image](../basics/image01.png)

In this picture above, the local administration loaned money from the bank. It received a one-time €400,000 money transfer from the bank, counted as income. That’s a key difference with private-sector accounting, where loans are **not** counted as income.

The debt that the administration contracted, by contrast, is a *stock*. It will appear on the balance sheet. Every year, the local administration will repay part of the debt stock to the bank. This will appear in the statement of income as debt repayment. The stock of debt will decrease by the same amount, and this will be visible in the balance sheet.


## Balance sheets of local administrations

It’s hard to do a balance sheet for a local administration. For companies, the balance sheet exists mostly to know how much the company is worth (so that it can be sold in case of a bankruptcy). This does not make sense for local administrations, because they cannot be sold! What’s more, local administrations often own assets that they cannot account for. For example, the Louvre Museum in Paris considers that the book value of the building itself is just 1€![^1] The book value is the accounting value of an asset, not its “real” value. Of course, the Louvre Museum is not really worth 1€. The balance sheet of a local administration shows the **book value** of its assets and liabilities.

[^1]: Source: [http://www.senat.fr/rap/r06-384/r06-38414.html](http://www.senat.fr/rap/r06-384/r06-38414.html) (in French)

The assets of a local administration represent the book value of things like buildings. The liabilities are the loans to be repaid, for instance. Unlike a company, a local administration has no equity. Instead, the difference between what the administration owns and what it owes is called *capitalization account*.

![Image](../basics/image05.png)


## Budgets, execution and realization

Unlike companies, local administrations have budgets to plan ahead. The budgets can be either voted, if the administration has an assembly, such as a city council, or can be decided by the administration itself if it has no such council (a museum, for instance). The budget tells the different departments of the administration what they can spend in a given year and it estimates what the administration will take in in taxes and other income. It must be voted before the year starts: The budget for 2017 must be decided in 2016. Most local administrations budget from January 1st to December 31st, but some might adopt other reporting periods (the government of the United Kingdom, for instance, reports from April 1st to March 31st).

If politicians want to change priorities after the budget was voted, they can easily vote modifications of the budget.

After the year has ended, the books are presented to the assembly or to the higher-level administration and must be approved.

To sum up, when working on public expenditures, you will face several types of budgets:

* The initial or preliminary budget, which is a plan.
* The amending budgets, which are plans made after the initial budget.
* The executed budget, or annual accounts, which presents what actually happened during the year. This is the most interesting document when you want to follow the money.
