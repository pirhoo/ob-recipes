Title: The Police Delice
Summary: The sale of confidential information never tasted so sweet.
Lang: en
Slug: the-police-delice
Ingredients: 
	Employment in the public sector with access to confidential information
Serves: Up to €1 million
Risk: Low
Duration: 10 years

**1. Get a job in the police.**
Access a position with access to confidential information. The police and the judiciary are obvious targets, but the public tender department is also full of yummy secrets.

**2. Wait for criminals to contact you.** Of course, you can take secret information and try to find a buyer yourself. But that's too risky! What if one of your prospective buyer is actually an undercover agent?

**3. Collect the cash.** Arrange for the payment (in cash!) to be made in discreet locations, like a forest, and hand over your material.

**4. Hide your tracks.** Most police databases track who accesses them. Xerox machines and printers tend to save a lot of data. Be careful when accessing and copying your material. If you do things right, the only risk you'll take will be that other police personnel find out how your buyer manages to evade them so easily. Use a middleman to stay safe.


!!! inreallife "In real life"
In **Germany**, the criminal police of Mecklenburg-Vorpommern is [suspected of having sold information](https://www.ndr.de/nachrichten/mecklenburg-vorpommern/Korruptionsverdacht-beim-LKA-Mecklenburg-Vorpommern,korruption200.html) to a Ukrainian criminal worth half a million euros.

In **France**, a police officer would call people under investigation and give them details on police work. He was suspected of selling such information, but was only convicted of sharing it. Read more in [Républicain Lorrain](http://www.republicain-lorrain.fr/edition-de-forbach/2016/12/14/behren-l-ancien-commandant-de-gendarmerie-condamne-a-trois-ans-de-prison-dont-18-mois-ferme?preview=true).

In **Québec**, a policeman were found to [have sold information](http://www.journaldequebec.com/2016/07/07/deux-enqueteurs-du-spvm-accuses-au-criminel) to the Hell's Angels worth C$125,000. Another one tried to sell a list of 1,500 informants for C$1 million.

!!! whatolookfor "What to look for"
Such schemes cannot be traced by journalists. Only a suspicion (why is this criminal never caught? why is this company always winning public tenders?) can lead you to asking the right questions. Even then, obtaining proof of wrongdoing will be extremely hard.