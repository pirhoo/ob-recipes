Title: The Confit of Interest
Summary: Mix together private interest and public money for the yummiest of all recipes!
Tags: highlight, main course
Slug: conflict-of-interest
Lang: en
Risk: Low
Duration: Under 5 years
Ingredients: 
	A public works company
    A high-ranking position in a civil administration
Serves: up to €10 million


**1. Access a position of power within a public administration**. You do not have to be in control of finances, but you need to have enough power to exchange favors. Being in the bureau of your party is a good choice, because you can decide who gets the top spot on the list on election day. 

**2. (Optional) Change the nominal owner of your company.** You can give your company to your wife or to a shell company in a tax-heaven (in a tax heaven that does not disclose the name of the owner). You can also steal the passport of a random person (or find one on Google Images) and create a company in Delaware under this person's name.

**3. Receive public contracts.** The key to successfully cook the Conflit of Interest is to make sure that you don't ask for anyone to give your company public contracts. It has to feel natural. Decision-makers within the administration know that you will return the favor if they award contracts to your company, but the precise terms of the exchange must never be spoken out loud.

**4. Enjoy, it's all legal!** After all, who can complain that someone does business and creates jobs? And, if you completed step 2, you'll probably be able to keep your secret until the statute of limitations runs out. Be careful that in some countries (Albania), members of parliaments do not have the right to profit from public contracts.


!!! inreallife "In real life"
    In *Albania*, Koço Kokëdhima, an member of Parliament, made sure his company
    ABISSNET was awarded over 150 public contracts. The conflict of interest was
    revealed by Open Spending Albania and the Albanian Institute of Science, two
    independent organizations.