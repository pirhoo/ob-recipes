Title: The Emphyteutic Scam
Summary: Sometimes the best recipes come with the most unappetizing names. Not only will the emphyteutic lease delight your friends, it is also risk-free!
Tags: highlight, main, course
Slug: the-emphyteutic-scam
Lang: en
Ingredients: 
	A plot of public land
    A non-profit organization
Serves: up to €10 millions
Risk: None
Duration: Unlimited


**1. Lease the land to your friends’ non-profit using an emphyteutic lease.** An emphyteutic lease[^1] means that the renters can use the land as they want for 99 years, but whatever they build belongs to the landlord after the lease ends. It’s a great way to motivate renters to build on the land (they’ll enjoy it until they die!) while landowners see their plot gain value.

**2. Subsidize the construction of a building for your friend.** Ask your friends to apply for a subsidy with your local authority so they can build offices, housing units or even sports fields on the land. Make it sound like the building has some public-service purpose - it will make it easier for you if journalists put their nose in. And remember: it has to be something that cannot be moved!

**3. Cancel the lease.** The lease was contracted for 99 years, remember? If you cancel it before, you have to compensate the renters, it’s only fair. And if the renters built something on the land you own, you have to buy it from them. You were only going to get it for free if you waited 99 years! At this step, you need to pay your friends the value of the buildings on the land. We advise you to wait a bit between steps 2 and 3, but don’t let it cool down more than 4 years!

If you completed step 2, that means that you actually paid twice for the buildings, but that’s perfectly normal.

**4. Sign a new, normal lease.** You weren’t going to evict your friends, were you? You can sign a new contract right after you cancel the emphyteutic lease, so that your friends don’t have to move. Make sure to charge a sweet rent, you don’t want your friends to waste all their new money! 

At this point, your friends have had a cash inflow equal to the value of the buildings that you paid for them. And - here comes the yummy part - it’s all legal!

[^1]: It comes with different names in different countries, with slight legal differences. Erbbaurecht in Germany, land lease in the United Kingdom, droit de superficie in Switzerland etc.


!!! inreallife "In real life"
    The football club of Strasbourg, France, used this recipe in 2010 and received €5 millions from the city council.
