Title: Buffet of the Parliament
Summary: Subsidize your lifestyle with these variations over expenses.
Lang: en
Slug: buffet-of-the-parliament
Ingredients: A mandate as elected official
Duration: As long as you are elected
Serves: €5000 to €30,000 / month
Risk: Very low

**1. Get yourself elected as an MP.** France is a good candidate but this recipe is known to work in other European countries.

**2. Beside your monthly paycheck (€5,400 after-tax in France) you have now access to parliamentary expense allowances.** You can use these allowances to cover your costs of having a constituency office and staff to run it, a main home in your constituency, and travels to and from Parliament. This is very basic but let's be more creative and impress your friends and family with a complete buffet.


* **Salad bar: Invest in real estate**

If it's your first experience with this recipe, don't buy property yourself but ask your kids to do so. Then pay them a rent and enjoy your new place. Publicly, rather speak of a "working studio" though.

* **International dishes: Travel the world in business class**

There's a lot of travel that comes with being an MP and it's especially true if you are a Member of European Parliament (MEP).
But no need to worry, because travel expenses are covered. Not just train and plane, as a MEP you can claim back for first class and business class tickets. Of course it's only for official trips to Brussels or Strasbourg but there is an extra travel allowance of up to €4,243 for trips outside of the EU that you can use for whatever official business. You are not going to celebrate your wedding anniversary in Bruxelles are you?

* **Dessert: Get a pool in your main home**

As an MP, it's important for you to be relaxed and work in good conditions. We suggest you to invest in a heated pool (think winter), spa or sauna. Make sure to relate the costs to your constituency office. And don't forget to pay your property tax with the allowances!


* **Kids' Meal: Get a nanny for free**

It can be difficult to be concentrated on your work while your kids are playing in the swimming pool of your office. Don't worry, your allowances allow you to hire people. Hire a nanny then! But make sure to insist on the fact that this person is doing "secretarial work".


* **Chutneys: Hire your life partner or any member of your family as a collaborator**

It would not be fair to be the only one in the family to benefit from the allowances!


!!! inreallife "In real life"
The United Kingdom the parliamentary expenses affair was a [major political scandal that emerged in 2009](https://en.wikipedia.org/wiki/United_Kingdom_parliamentary_expenses_scandal) concerning expenses claimed by British MPs over the previous years. Abuses ranged from minor reimbursements likes gardening equipment purchases or cat food, to the realization of capital gains in real estate housing purchased with public funds.

In Germany, members of parliament are given 12,000€ a year for office supplies with no oversight (on top of a 4,000€ monthly allowance for daily expenditures). Unsurprisingly, they were [caught buying luxury pens](http://www.spiegel.de/politik/deutschland/bundestag-abgeordnete-kauften-luxus-fueller-fuer-68-000-euro-a-662532.html) and other Christmas presents at the end of the year.

In France, Georges Ginesta used his allowances [to buy a villa with a pool](http://www.politique.net/2014071601-saint-raphael-permanence-villa.htm) when he was a member of the National Assembly of France.

In United Kingdom, MP Tory chairman Caroline Spelman admits she used her allowance [to pay her nanny for secretarial work.](http://news.bbc.co.uk/2/hi/uk_news/politics/7441360.stm)

In Japan, assembly members in Toyama, a city of half a million, reportedly [falsified expense claims by using their personal computers to forge receipts or adding a digit to the numbers on receipts.](http://www.asahi.com/ajw/articles/AJ201609190037.html)


!!! whatolookfor "What to look for"
In most countries, public authorities rather than individuals are covered by the The Freedom of Information laws (FOI). This means that the Members are under no obligation to respond to any request for information made to them as an individual. Plus, as the system of allowances is organized and administered by the MPs themselves, it's unlikely to change. However, you can query the Parliament itself for the payments it made to individual MPs.

