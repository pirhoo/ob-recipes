Title: The Oil Banquet
Summary: For a large party, a banquet is de rigueur. With a large oil company and a few friends, you can cook billions and feed hundreds.
Tags: highlight
Slug: the-oil-banquet
Lang: en
Ingredients: A publicly-owned oil company
             A ministerial position
Serves: over €1 billion
Risk: Low
Duration: 50 years


**1. Get yourself appointed in a ministerial job** or an equivalent position in a state that owns an oil company. France and Brazil are good candidates, although this recipe is known to local prosecutors.

**2. Find a henchman.** Your henchman must have the outlook of a businessman and a stable family life that makes him respectable and a criminal past that makes him useful for this recipe.

Henchwomen might be considered, but in the public history of public budget cooking, not one has been found.

**3. Funnel kickbacks to a slush fund.** [Read the recipe on kickbacks to get started](/the-kickbacks).

Oil companies manage large contracts. Asking for a commission of a few percent of the total price on a deal to build a refinery can yield hundreds of millions. Asking a foreign leader to increase the price of the oil he sells to your company by a few dollars per barrel and then send back this surplus to a slush fund is also an option.

Make sure that your henchman takes care of these deals, not you! You can put him in charge of the slush fund, too.

**4. Serve the party**. The slush fund can hire your friends or your mistresses, who, in turn, can give you small gifts as a token of gratitude. It can buy you houses in Rio or Paris. It can pay for your political campaigns or the campaigns of your friends.

**5. Remain too big to jail**. Book-cooking on this scale is only possible if you make sure that public prosecutors cannot reach you. Your henchman must be trustworthy enough not to talk and, in case of an arrest, to eat the memory chip of his cell phone (and chew on it, too). 

![The refinery of Port-Gentil, Gabon.](../recipes/port-gentil.jpg)
<small>Some see a refinery, others see a promising ingredient for a yummy recipe.</small>

!!! inreallife "In real life"
    This recipe is based on the [Elf affair](http://www.legalaffairs.org/issues/May-June-2002/story_ignatius_mayjun2002.html) in France and on the [Petrobras affair](https://pt.wikipedia.org/wiki/Operação_Lava_Jato) in Brazil, both of which fed hundreds of local and foreign politicians. Alfred Sirven and Alberto Youssef, the henchmen, were both respectable businessmen with a criminal past. Sirven ate his SIM card upon arrest, Youssef did not.
