Title: A Good Ol' Bribe
Summary: Ten 100-euro notes, an enveloppe, a meeting in the back room of a bar... The good ol' bribe can be mouth-watering!
Lang: en
Slug: good-ol-bribe
Ingredients: 
	A position in a public administration with direct contact to citizens
Serves: €500 to €5,000
Risk: High
Duration: 20 years

**1. Get a position where your work impacts citizens directly.** The easiest way to ask for bribes is to work in social housing, in a position where you can decide who receives an apartment. You can also use this recipe if you control personal vehicles (_TÜV_ in Germany, _MOT_ in the United Kingdom, _contrôle technique_ in France).

**2. When someone comes to you, imply that things can be speeded up**. Don't ask for a bribe directly (that would be extortion). Instead, let it be known that the waiting time could be shortened, or that the control could be less tight, in exchange for a small gift. 

For a flat in social housing, you can ask for up to 5,000€ (per flat). For a technical check of a vehicle, prices are much lower and asking for more than 100€ would put you in trouble.

**3. Arrange the transfer of money in cash.** You can meet at a café to collect the envelope containing the banknotes. Transfers are usually split in two: part before you take action and part after you did.

For technical checks, the gift can be left on the passenger seat of the vehicle, thus providing both sides of the transaction with plausible deniability. (Plausible deniability means that you can pretend that the gift was not intended as a bribe but was something totally different.)

**4. Be careful.** This recipe can give you a sweet supplementary income. But you have to be careful not to leave traces (always ask for gifts in cash!), not to be seen (don't meet in the café next to your office) and share some of your revenues with colleagues, so they don't blow the whistle on you.

!!! inreallife "In real life"
In **France**, employees of the social housing companies of Nanterre ([read](http://www.lexpress.fr/actualites/1/societe/un-ex-responsable-des-hlm-des-hauts-de-seine-condamne-a-deux-ans-de-prison_1314840.html)) and Sarcelles ([read](Sarcelles http://www.20minutes.fr/societe/591636-20100901-societe-sarcelles-pots-de-vin-contre-hlm)) have been indicted and sentenced for taking bribes.

In **Germany**, a bribe-against-social housing scheme was uncovered in Leipzig ([read](http://www.mdr.de/exakt/schmiergeld-100.html)). In Mainz, an employee of the local car control subsidiary was convicted of collecting 10,000€ in bribes to turn a blind eye on 190 cars ([read](http://www.fr-online.de/rhein-main/schmiergeldaffaere-beim-tuev-in-mainz-korrupter-pruefer-vor-gericht,1472796,7140974.html))

!!! whatolookfor "What to look for"
Such bribes do not show up in any public document. The best way to uncover a system of bribes is to ask people who are looking for social housing if they have been asked to pay bribes, or to pose as a person looking for public housing and act undercover.