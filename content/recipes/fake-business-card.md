Title: The Fake Business Card
Summary: One ingredient, infinite possibilites for this recipe that will allow you to be whoever you want to be (and rich).
Slug: the-fake-business-card
Lang: en
Tags: highlight
Ingredients: 
	Helping hands in the executive board of a state owned company
Serves: €500,000 to €5 million
Risk: Low
Duration: Less that 5 years

**1. Create a company with a name that sounds like official "public work".** The name of the company should contains words that really match your future position (like *engineering and consulting Ltd* or *architectural practice GmbH*). Be sure to open a bank account for the company. 

**2. Print business cards with your name, the name of your company and the position of your choice.** Don’t forget to add a grand-sounding title like "PhD" or "Prof" to your business card.

**3. With your brand new very serious title, ask your friends in state-owned company to hire you as a contractor** for their new project.

**4. Wait at least until you have the downpayment on the bank account of your company.** You can fake it until you make it, but as the goal is not really to build anything (remember, you are not really an architect), follow the next steps.

**5. Ask one of your friends at the board to fire you and to make it known loudly.** If journalists ask, say that no one asked about your university qualifications and that it wasn’t necessary for the work to be carried out.
Then, your friend is the one that saved the public work from a disaster, and you are a victim of a failure at the human resources department. 

**6. Enjoy your money!**  It cost you only the price of printing few business cards. 

**Warning:** Be careful to avoid using terms related to regulated professions like law and medicine on your business card. Otherwise it's risk free as long as you make sure that no one asks you for a copy of your diploma!

!!! inreallife "In real life"
In 2014, Alfredo Di Mauro, [the man in charge of the smoke extraction system at Berlin Brandenburg Airport (BER) was fired](http://www.dailymail.co.uk/travel/article-2671133/Berlin-airport-hired-fake-engineer-design-fire-safety-delayed-4bn-project-TWO-YEARS.html) and admitted that he was not an engineer, despite claiming this on his business cards. It's unclear weather Di Mauro is actually friend with the CEO of BER.

It wasn't the first time that Di Mauro used this recipe. In 2002, [he pretented to be an architect and was engaged in the construction of a medical center](https://www.rt.com/news/168508-berlin-airport-fake-engineer/) in the German town of Offenbach.

Both projects took a disastrous turn but no link has been made between the billions lost and the “error that occured” on Di Mauro’s business cards, as he says himself. 

(An [investigation by Der Spiegel](http://www.spiegel.de/spiegel/berlin-flughafen-ber-wie-aus-dem-grossprojekt-ein-debakel-wurde-a-1163742.html) found that Di Mauro's skills were excellent and that he served as a scapegoat for the airport's woes. However, it did not dispute his lack of credentials.)

!!! whatolookfor "What to look for"
If the people hired in a very high position are not registered on Linkedin, Xing or any other professional social network, it's strange. If their company doesn't have a website, it starts to be suspicious. If on top of that, Google Street View shows a family villa with pool located at the address of the company, then you probably have a Chef who is a specialist of this recipe. 
