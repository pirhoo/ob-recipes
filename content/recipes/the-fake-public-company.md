Title: The Fake Public Company
Summary: With just a hint of creativity, this recipe can become your favorite!
Slug: the-fake-public-company
Lang: en
Ingredients: 
	Access to your public body’s means of payment
Serves: up to €10 million
Risk: High
Duration: 30 years

**1. Get yourself elected or nominated to the position of treasurer** of a public administration. 

**2. Create a company with an official-sounding name** and open a bank account for the company. The name of the company should really match a public body, like Reserve Sewage Development Fund. If you need to cook a lot, you can create several.

**3. Write checks to your fake public company.** If your reputation is impeccable and the names of your companies good enough, chances are no one will suspect anything. They might even praise your honesty!

**4. Enjoy responsibly.** This recipe can be used for years in a row. However, remember that you're supposed to receive a public servant's salary, so don't show off! 


!!! inreallife "In real life"
    In the **United States**, [Rita Crundwell](https://en.wikipedia.org/wiki/Rita_Crundwell) diverted over $50 millions from the town of Dixon, Illinois, over 22 years. Had she not owned 400 race horses, she might have even retired in peace.

    In Germany, an employee of the Bavarian town of Schweinfurt [transfered close to 300,000€](http://www.radioprimaton.de/2016/03/02/oeffentliche-mittel-veruntreut-stadt-schweinfurt-erstattet-anzeige/) to bank accounts she controlled. She was in charge of continuing education and gave grants to fictitious persons, whose bank accounts she had access to.

!!! whatolookfor "What to look for"
	For large administrations, such schemes can be spotted as [conflicts of interest](http://www.cookingbudgets.com/conflict-of-interest/). In smaller ones, one would need to access the detailed list of payments to identify problematic transactions. Unfortunately, such documents are rarely made public.