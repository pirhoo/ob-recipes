Title: The Kickbacks
Summary: It's like passing plates around, but with cash!
Tags: highlight, main course
Slug: the-kickbacks
Lang: en
Ingredients: Authority to award a public contract
Serves: up to €100 million
Duration: 10 years
Risk: Medium


**1. Get yourself in a position where you can award public contracts.** Not a clerical position where you sign the contract, no, to a real position of power. At the local level, that means director of public works. At the national level, minister for defense is a good choice. 

**2. Meet the heads of the companies that earn public contracts** and make sure they understand that they can charge more than needed, in exchange for a giving you some of the money back (the _kickback_). This meeting should not be registered anywhere, of course.

Paying back 3% of a contract is considered normal in many European countries. 

**3. Enjoy the kickbacks.** At the local level, they take the form of envelopes stuffed with cash that local companies give you, usually at a yearly party or over dinner in a restaurant. At the national level, you will need a henchman to carry the cash around (in suitcases, not envelopes!).

The kickbacks can improve your lifestyle or finance your next political campaign, it's up to you!

**4. Keep the relation well-oiled.** For the kickbacks to keep coming, you need to make sure the companies that support you keep getting contracts. If they don't, they might get angry.


!!! inreallife "In real life"
    In **The Netherlands**, a company was paid €2 million to do work worth €1m as part of the _Betuweroute_, a railway track (read the story, in Dutch, [at NRC.nl](http://www.nrc.nl/handelsblad/2004/09/10/grote-bouwfraude-betuwelijn-en-hsl-7701203)).

    In **France** the French government paid kickbacks to the decision-makers of [Pakistan, Saudi Arabia](https://fr.wikipedia.org/wiki/Affaire_des_fr%C3%A9gates_d%27Arabie_saoudite_et_des_sous-marins_du_Pakistan)> or [Taiwan](https://fr.wikipedia.org/wiki/Affaire_des_fr%C3%A9gates_de_Ta%C3%AFwan) to sell them military equipment. Beneficiaries of these kickbacks paid their own kickbacks to the French decision-makers. The money was used to finance the presidential campaign of Edouard Balladur, for instance. The 2002 attack in Karachi, which killed 12 French nationals, is believed to be linked to the non-payment of part of the kickbacks.

    In **Québec**, Canada, a report published in 2015 showed that, in the 2000's, it was very common for public officials in the city of Montréal to receive 3% of all public contracts ([read the report in French](https://www.ceic.gouv.qc.ca/fileadmin/Fichiers_client/fichiers/Rapport_final/Rapport_final_CEIC_Integral_c.pdf)).  

!!! whatolookfor "What to look for"
	Most public works have benchmarks (e.g the price of 1km of highway) which you can obtain by calling an expert in the field under investigation. If the public contract to do the public work exceeds the benchmark greatly, you can become suspicious. For one-time deals for which no benchmark exists (sale of a submarine, construction of a giant bridge), it's near impossible to detect fraud without direct contacts with people part to the deal.
