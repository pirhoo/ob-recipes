## Installation

    make install
    
## Building the site

    make html
    
## Running a local server

    make devserver
    
(runs on port 8000)

The local server reloads the active browser session on any changes to the local templates.

However, on editing the theme (in `themes/recipes`), you will need to run `make html` and reload the browser window.

## Press coverage

Libération, 10 October 2016, [link](http://www.liberation.fr/direct/element/_49128/)
Journalism.co.uk, 12 October 2016, [link](https://www.journalism.co.uk/news/tool-for-journalists-recipes-for-cooking-public-budgets-and-investigating-corruption/s2/a682013/)